from django.shortcuts import render
from date.time import date

# Enter your name here
mhs_name = 'Nurdela Ardiansyah' # TODO Implement this

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(1998)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    today = date.today()
    return today.year - birth_year
